﻿using System;
using System.Threading;

namespace clock
{
    class Program
    {
        private void Time() {
            for (; ; ) {
                Console.WriteLine(DateTime.Now.ToString());
                Console.WriteLine("\a");
                Thread.Sleep(1000);
                Console.Clear();
            }
        }
        static void Main(string[] args) {
            Program PC = new Program();
            ThreadStart TS = new ThreadStart(PC.Time);
            Thread t = new Thread(TS);
            t.Start();
            GC.Collect();
        }
    }
}
